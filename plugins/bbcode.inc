<?php

/**
 * The BBCode converter is from jcomments Joomla! component
 *
 * Modify this function to fit your need/custom BBcodes
 */

define('JCOMMENTS_PCRE_UTF8', 'u');

function j2d_bbcode_convert($str) {
	$patterns = array();
	$replacements = array();

	// B
	$patterns[] = '/\[b\](.*?)\[\/b\]/i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '<b>\\1</b>';

	// I
	$patterns[] = '/\[i\](.*?)\[\/i\]/i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '<i>\\1</i>';

	// U
	$patterns[] = '/\[u\](.*?)\[\/u\]/i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '<u>\\1</u>';

	// S
	$patterns[] = '/\[s\](.*?)\[\/s\]/i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '<strike>\\1</strike>';

	// SUP
	$patterns[] = '/\[sup\](.*?)\[\/sup\]/i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '<sup>\\1</sup>';

	// SUB
	$patterns[] = '/\[sub\](.*?)\[\/sub\]/i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '<sub>\\1</sub>';

	// URL (local)
	global $base_url;
	$patterns[] = '#\[url\]('.preg_quote($base_url, '#').'[^\s<\"\']*?)\[\/url\]#i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '<a href="\\1" target="_blank">\\1</a>';

	$patterns[] = '#\[url=('.preg_quote($base_url, '#').'[^\s<\"\'\]]*?)\](.*?)\[\/url\]#i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '<a href="\\1" target="_blank">\\2</a>';

	$patterns[] = '/\[url=(\#|\/)([^\s<\"\'\]]*?)\](.*?)\[\/url\]/i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '<a href="\\1\\2" target="_blank">\\3</a>';

	// URL (external)
	$patterns[] = '#\[url\](http:\/\/)?([^\s<\"\']*?)\[\/url\]#i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '<a href="http://\\2" rel="external nofollow" target="_blank">\\2</a>';

	$patterns[] = '/\[url=([a-z]*\:\/\/)([^\s<\"\'\]]*?)\](.*?)\[\/url\]/i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '<a href="\\1\\2" rel="external nofollow" target="_blank">\\3</a>';

	$patterns[] = '/\[url=([^\s<\"\'\]]*?)\](.*?)\[\/url\]/i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '<a href="http://\\1" rel="external nofollow" target="_blank">\\2</a>';

	$patterns[] = '#\[url\](.*?)\[\/url\]#i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '\\1';

	// EMAIL
	$patterns[] = '#\[email\]([^\s\<\>\(\)\"\'\[\]]*?)\[\/email\]#i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '\\1';

	// IMG
	$patterns[] = '#\[img\](http:\/\/)?([^\s\<\>\(\)\"\']*?)\[\/img\]#i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '<img class="img" src="http://\\2" alt="" border="0" />';

	$patterns[] = '#\[img\](.*?)\[\/img\]#i' . JCOMMENTS_PCRE_UTF8;
	$replacements[] = '\\1';

	// CODE
	$codePattern = '#\[code\=?([a-z0-9]*?)\](.*?)\[\/code\]#ism' . JCOMMENTS_PCRE_UTF8;

	$patterns[] = $codePattern;
	$replacements[] = '<span class="code">'.t('CODE').'</span><code>\\2</code>';

	if (!function_exists('jcommentsProcessCode')) {
		function jcommentsProcessCode($matches) {
			return (htmlspecialchars(trim($matches[0])));
		}
	}

	$str = preg_replace_callback($codePattern, 'jcommentsProcessCode', $str);
	$str = preg_replace($patterns, $replacements, $str);

	// QUOTE
	$quotePattern = '#\[quote\s?name=\"([^\"\'\<\>\(\)]+)+\"\](<br\s?\/?\>)*(.*?)(<br\s?\/?\>)*\[\/quote\]#i' . JCOMMENTS_PCRE_UTF8;
	$quoteReplace = '<span class="quote">Quoting \\1</span><blockquote>\\3</blockquote>';
	while(preg_match($quotePattern, $str)) {
		$str = preg_replace($quotePattern, $quoteReplace, $str);
	}
	$quotePattern = '#\[quote[^\]]*?\](<br\s?\/?\>)*([^\[]+)(<br\s?\/?\>)*\[\/quote\]#i' . JCOMMENTS_PCRE_UTF8;
	$quoteReplace = '<span class="quote">'.t('QUOTE_SINGLE').'</span><blockquote>\\2</blockquote>';
	while(preg_match($quotePattern, $str)) {
		$str = preg_replace($quotePattern, $quoteReplace, $str);
	}

	// LIST
	$matches = array();
	$matchCount = preg_match_all('#\[list\](<br\s?\/?\>)*(.*?)(<br\s?\/?\>)*\[\/list\]#is' . JCOMMENTS_PCRE_UTF8, $str, $matches);
	for ($i = 0; $i < $matchCount; $i++) {
		$textBefore = preg_quote($matches[2][$i]);
		$textAfter = preg_replace('#(<br\s?\/?\>)*\[\*\](<br\s?\/?\>)*#is' . JCOMMENTS_PCRE_UTF8, "</li><li>", $matches[2][$i]);
		$textAfter = preg_replace("#^</?li>#" . JCOMMENTS_PCRE_UTF8, "", $textAfter);
		$textAfter = str_replace("\n</li>", "</li>", $textAfter."</li>");
		$str = preg_replace('#\[list\](<br\s?\/?\>)*' . $textBefore . '(<br\s?\/?\>)*\[/list\]#is' . JCOMMENTS_PCRE_UTF8, "<ul>$textAfter</ul>", $str);
	}
	$matches = array();
	$matchCount = preg_match_all('#\[list=(a|A|i|I|1)\](<br\s?\/?\>)*(.*?)(<br\s?\/?\>)*\[\/list\]#is' . JCOMMENTS_PCRE_UTF8, $str, $matches);
	for ($i = 0; $i < $matchCount; $i++) {
		$textBefore = preg_quote($matches[3][$i]);
		$textAfter = preg_replace('#(<br\s?\/?\>)*\[\*\](<br\s?\/?\>)*#is' . JCOMMENTS_PCRE_UTF8, "</li><li>", $matches[3][$i]);
		$textAfter = preg_replace("#^</?li>#" . JCOMMENTS_PCRE_UTF8, '', $textAfter);
		$textAfter = str_replace("\n</li>", "</li>", $textAfter."</li>");
		$str = preg_replace('#\[list=(a|A|i|I|1)\](<br\s?\/?\>)*' . $textBefore . '(<br\s?\/?\>)*\[/list\]#is' . JCOMMENTS_PCRE_UTF8, "<ol type=\\1>$textAfter</ol>", $str);
	}

	$str = preg_replace('#\[\/?(b|i|u|s|sup|sub|url|img|list|quote|code|hide)\]#' . JCOMMENTS_PCRE_UTF8, '', $str);
	unset($matches);
	return $str;
}
