<?php

function j2d_ttcn_createcck(&$context) {
  global $_joomla2drupal_vars;
  
  db_query('DROP TABLE IF EXISTS {j2d_temp}');
 	// $path = drupal_get_path('module', 'joomla2drupal') .'/plugins';
 	// _joomla2drupal_create_content(file_get_contents($path .'/cck-gallery.txt'));
 	// _joomla2drupal_create_content(file_get_contents($path .'/cck-software.txt'));
 	// _joomla2drupal_create_content(file_get_contents($path .'/cck-video.txt')); 
}

function j2d_ttcn_mosvideo(&$matches) {
  $mpos = strpos($matches[2], '#');
  if ($mpos !== false) {
    $title = substr($matches[2], $mpos+1);
    $matches[2] = substr($matches[2], 0, $mpos);
  }
  if ($matches[1] == 'url') {
    $url = $matches[2];
    //remove unnecessary parts
    $url = preg_replace('/\/watch\?v=([\&]*)/', '/watch?v=\1', $url);
    $url = preg_replace('/\/videoplay\?docid=([^\&]*)\&/', '/videoplay?docid=\1', $url);
  }
  else switch ($matches[1]) {
    case 'youtube':
      $url = 'http://www.youtube.com/watch?v='. $matches[2];
      break;
    case 'google':
      $url = 'http://video.google.com/videoplay?docid='. $matches[2];
      break;
    case 'dailymotion':
      $url = 'http://www.dailymotion.com/video/'. $matches[2];
      break;
  }
  return "\n<p>$url $title</p>\n";
}

function j2d_ttcn_mambot($text) {
  global $_joomla2drupal_vars;
  
  $text = preg_replace_callback('/{video ([a-z]+)=(.+?)}/s', 'j2d_ttcn_mosvideo', $text);
  $text = preg_replace('/{style type=leftside}(.+?){\/style}/s', '<div class="s-side">\1</div>', $text);
  $text = preg_replace('/{style type=moreinfo}(.+?){\/style}/s', '<div class="s-moreinfo">\1</div>', $text);
  // replace very old style links
  $text = preg_replace('/"(|http:\/\/www\.thongtincongnghe\.com\/)content\/view\/(\d+)[0-9\/]*/', '"http://www.thongtincongnghe.com/article/\2', $text);

  $text = preg_replace('/ttcnStyle(\w+)/', 's-\1', $text);

  return $text;
}

function j2d_ttcn_preprocess(&$context) {
  module_disable(array('workflow', 'workflow_access', 'token', 'pathauto'));
  $tables = array('field_soft_article_id', 'field_soft_license', 'field_soft_os', 'field_thumbnail', 'field_video_urls', 'type_gallery', 'type_software', 'type_video', 'type_story');
  foreach ($tables as $tbl) {
//    db_query('DROP TABLE IF EXISTS {content_'. $tbl .'}');
    _joomla2drupal_reset_table_('content_'.$tbl);
  }
}

function j2d_ttcn_postprocess(&$context) {
  global $_joomla2drupal_vars;

  module_enable(array('workflow', 'workflow_access', 'token', 'pathauto'));

  if (module_exists('path')) {
    $aliases = array(
      '1' => 'me/about',
    );
    foreach ($aliases as $path=>$alias) {
      _joomla2drupal_set_alias('node/'. $path, $alias);
    }
    _joomla2drupal_set_roles(63, array(3,4,5));//nemo
    _joomla2drupal_set_roles(64, array(3,4,5));//hainam
    drupal_clear_path_cache();
  }
  
  // change vocabulary name from 'Topics' to 'Chủ đề'
  $query = "Update {vocabulary} set name = 'Chủ đề' " .
           "where name = 'Topics'";
  $result = db_query($query);
}

function j2d_ttcn_update_term($old_term, $new_tid) {
  $result = db_query("SELECT tid FROM {term_data} WHERE name = '%s'", $old_term);
  $tids = array();
  while ($tid = db_fetch_array($result)) {
    $tids[] = $tid['tid'];
  }
  db_query("UPDATE {term_node} SET tid = %d WHERE tid IN (%b)", $new_tid, implode(',', $tids));
  foreach ($tids as $tid) {
    taxonomy_del_term($tid);
  }
}

