<?php

function j2d_gallery_batch(&$context) {
  j2d_gallery_create();
}

function j2d_gallery_create() {
  global $_joomla2drupal_vars;
  
  $result = db_query("SELECT * FROM {j2d_temp} WHERE type='gallery'");
  while ($item = db_fetch_array($result)) {
    $gallery = explode("\n", $item['data']);
    $node = new StdClass();
    $node->title = array_shift($gallery);
    $node->uid = array_shift($gallery);
    $node->comment = 0;
    $node->status = 1;
    $node->type = 'gallery';
    $node->field_gallery_urls[0]['value'] = implode("\n", $gallery);
    node_save($node);
    
    db_query('UPDATE {node_revisions} SET body = REPLACE(body, \'{mosgallery %d}\',\' [nid:%d]\') WHERE nid=%d', $item['id'], $node->nid, $item['node']);
  }
}

function j2d_gallery_save(&$post) {
  global $_joomla2drupal_vars;
  
  $map =& $_joomla2drupal_vars['map'];
  $gallery = array();
  foreach ($_joomla2drupal_vars['tmp']['img'] as $image) {
    $params = explode('|', $image);
    if ($params[2] == 'gallery') {
      $gallery[] = $params[0] .' '. $params[4];
    }
  }
  if (count($gallery) > 0 && preg_match('/{mosgallery(.*?)}/', $post['fulltext'], $match)) {
    $title = empty($match[1]) ? $post['title'] : $match[1];
    array_unshift($gallery, $title, joomla2drupal_convert_userid($post['created_by']));
    db_query('INSERT INTO {j2d_temp} (node, type, data) VALUES (%d, "gallery", "%s")', $post['id'], implode("\n", $gallery));
    $post['fulltext'] = preg_replace('/\{mosgallery(.*?)\}/', '{mosgallery '. db_last_insert_id('j2d_temp', 'id') .'}', $post['fulltext']);
  }
  return $post;
}