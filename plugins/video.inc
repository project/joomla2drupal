<?php

/**
 * Convert story node type into video
 *
 * @param unknown_type $context
 */
function j2d_video_batch(&$context) {
  global $_joomla2drupal_vars;

  $map =& $_joomla2drupal_vars['map'];
  $tid = $map['sec'][7];  // sec7: Video report
  $prefixes = array('http://www.youtube.com/watch?v=', 'http://video.google.com/videoplay?docid=', 'http://www.dailymotion.com/video/');
  foreach ($prefixes as $k=>$v) {
    $prefixes[$k] = preg_quote($v, '/');
  }
  $pattern = '/^\<p\>(('. implode('|', $prefixes) .').*)\<\/p\>$/m';

  $result = db_query('SELECT nid FROM {term_node} WHERE tid=%d', $tid);
  while ($nid = db_result($result)) {
    if (! $node = node_load($nid)) continue;

    $node->type = 'video';
    if (preg_match_all($pattern, $node->body, $matches, PREG_SET_ORDER) > 0) {
      $delta = 0;
      foreach ($matches as $match) {
        $node->field_video_urls[$delta++]['value'] = trim(strip_tags($match[1])); 
      }
    }
    $node->body = preg_replace($pattern, '<!--video-->', $node->body);
    $node->teaser = node_teaser($node->body);
    node_save($node);//this invokes the presave before
    db_query('DELETE FROM {content_type_story} WHERE nid=%d', $node->nid);
//    thumbnail_thumbnail_process($node); 
  }
}
