<?php

function j2d_sobi2_batch(&$context) {
  j2d_sobi2_terms();  
  j2d_sobi2_import();
}

function j2d_sobi2_terms() {
  global $_joomla2drupal_vars;

  $map =& $_joomla2drupal_vars['map'];
  $prefix = $_joomla2drupal_vars['config']['Joomla']['db_prefix'];

  joomla2drupal_db_joomla_();

  $result = db_query("SELECT * FROM %ssobi2_categories WHERE catid>1", $prefix);
  $cats = array();
  while ($row = db_fetch_array($result)) {
    $cats[] = $row;
  }

  joomla2drupal_db_drupal_();
  
  $edit = array();
  $edit['name'] = 'sobi2 categories';
  $edit['nodes']['software'] = 1;
  $edit['hierarchy'] = 0;
  $edit['tags'] = 0;
  $edit['multiple'] = 1;
  $edit['required'] = 1;
  $edit['weight'] = 0;
  taxonomy_save_vocabulary($edit);
  $map['vocab']['sobi2'] = $edit['vid'];

  $map['sobi2'] = array();

  foreach ($cats as $cat) {
    $edit = array();
    $edit['vid'] = $map['vocab']['sobi2'];
    $edit['parent'] = 0;
    $edit['name'] = $cat['name'];
    $edit['description'] = $cat['introtext'];
    $edit['synonyms'] = '';
    $edit['weight'] = 0;
    _joomla2drupal_taxonomy_save_term_($edit);
    $map['sobi2'][$cat['catid']] = $edit['tid'];
  }
}

function j2d_sobi2_import() {
  global $_joomla2drupal_vars;

  $map =& $_joomla2drupal_vars['map'];
  joomla2drupal_db_joomla_();
  $prefix = $_joomla2drupal_vars['config']['Joomla']['db_prefix'];

  $result = db_query("SELECT * FROM %ssobi2_cat_items_relations", $prefix);
  $rels = array();
  while ($row = db_fetch_array($result)) {
    if (! isset($rels[$row['itemid']])) $rels[$row['itemid']] = array();
    $rels[$row['itemid']][] = $row['catid'];
  }

  $result = db_query("SELECT * FROM %ssobi2_language WHERE sobi2Lang='vietnamese'", $prefix);
  $langs = array();
  $fields = array();
  while ($row = db_fetch_array($result)) {
    $langs[$row['langKey']] = $row['fieldid'];
    $fields[$row['fieldid']] = $row['langKey'];
    $values[$row['langKey']] = $row['langValue'];  
  }

  $result = db_query('SELECT * FROM %ssobi2_item ORDER BY itemid', $prefix);
  $items = array();
  while ($row = db_fetch_array($result)) {
    $res = db_query("SELECT fieldid, data_txt FROM %ssobi2_fields_data WHERE itemid=%d", $prefix, $row['itemid']);
    while ($data = db_fetch_array($res)) {
      $row['fields'][$data['fieldid']][] = $data['data_txt'];
    }
    $items[] = $row;
  }
  
  //the review plugin
  $result = db_query('SELECT * FROM %ssobi2_plugin_reviews ORDER BY itemid, added', $prefix);
  $comments = array();
  while ($row = db_fetch_array($result)) {
    $comments[] = $row;
  }
  watchdog('comment', count($comments) .' comments found for SOBI2');

  joomla2drupal_db_drupal_();
  module_enable(array('nodereference', 'optionwidgets', 'filefield'));
  
  // get unused id to save our series
  $result = db_query('SELECT nid FROM {node}');
  $nodes = array();
  while ($node = db_fetch_array($result)) {
    $nodes[] = $node['nid'];
  }
  $max = $nodes[count($nodes)-1];
  $unused = array_diff(range(1, $max), $nodes);
  
  foreach ($items as $item) {
    $node = new StdClass();
    $node->type = 'software';
    $node->status = 1;  // published
    $node->comment = 2;
    $node->uid = joomla2drupal_convert_userid($item['owner']);
    $node->title = $item['title'];
    $node->teaser = $item['fields'][$langs['field_intro']][0];
    $node->body = stripslashes(stripslashes($item['fields'][$langs['field_description']][0]));
    $node->field_soft_home_url[0]['value'] = $item['fields'][$langs['field_homepage']][0];
    $node->field_soft_author[0]['value'] = $item['fields'][$langs['field_author']][0];
    $node->field_soft_download_url[0]['value'] = $item['fields'][$langs['field_download']][0];
    $node->field_soft_version[0]['value'] = $item['fields'][$langs['field_version']][0];
    $node->field_soft_article_id[0]['nid'] = intval(preg_replace('/^.*\/(\d+)$/', '\1', $item['fields'][$langs['field_article']][0]));
    $node->field_soft_update[0]['value'] = $item['fields'][$langs['field_updatetime']][0];

    //thumbnail
    if (! empty($item['image'])) {
      $file = joomla2drupal_create_file('images/com_sobi2/clients/'. $item['image'], 'software/'.$item['image'], $item['owner']);
      $node->field_soft_thumbnail[0] = array(
        'fid' => $file->fid,
        'list' => '1',
        'data' => 'a:3:{s:11:"description";s:0:"";s:3:"alt";s:0:"";s:5:"title";s:0:"";}',
      );
    }

    $i = 0;
    foreach ($item['fields'][$langs['field_license']] as $lic) {
      $node->field_soft_license[$i++]['value'] = $values[$lic];
    }
    
    $i = 0;
    foreach ($item['fields'][$langs['field_os']] as $os) {
      $node->field_soft_os[$i++]['value'] = $values[$os];
    }
        
    // Save the node out to the database
    node_save($node);
    $create = strtotime($item['publish_up']);
    $update = strtotime($item['last_update']);
    db_query('UPDATE {node} SET created=%d, changed=%d WHERE nid=%d', $create, $update, $node->nid);
    db_query('UPDATE {node_revisions} SET timestamp=%d WHERE nid=%d', $update, $node->nid);
    
    if (count($unused) > 0) {
      $id = array_shift($unused);
      db_query('UPDATE {node} SET nid=%d WHERE nid=%d', $id, $node->nid);
      db_query('UPDATE {node_revisions} SET nid=%d WHERE nid=%d', $id, $node->nid);
      $node->nid = $id;
    }
    
    //$node->vid = db_last_insert_id('node_revisions', 'vid');
    foreach ($rels[$item['itemid']] as $cat) {
      _joomla2drupal_attach_term_($node, $map['sobi2'][$cat]);
    }
    
    _joomla2drupal_set_alias('node/'.$node->nid, 'software/'.$item['itemid']);
    
    while (count($comments) > 0 && $comments[0]['itemid'] == $item['itemid']) {
      $comment = array_shift($comments);
      if (empty($comment['title']) && empty($comment['review'])) continue;
      $edit = array(
        'nid'  => $node->nid,
        'uid'  => joomla2drupal_convert_userid($comment['userid']),
        'subject'  => $comment['title'],
        'comment'  => $comment['review'],
        'status'  => 1 - $comment['published'],
        'name'  => $comment['username'],
        'mail'  => $comment['email'],
        'timestamp' => joomla2drupal_date_2_timestamp_($comment['added']),
        'ip' => $comment['ip'],
      );
  
      _joomla2drupal_comment_save_($edit);
    }
  }
}