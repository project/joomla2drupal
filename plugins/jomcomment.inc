<?php

/**
 * Import comments
 *
 * @param unknown_type $context
 */
function j2d_jomcomment_batch(&$context) {
  global $_joomla2drupal_vars;

  $map =& $_joomla2drupal_vars['map'];
  if (! isset($_joomla2drupal_vars['sandbox']['comment'])) { 
    $_joomla2drupal_vars['sandbox']['comment'] = 0;
  }
  $start = $_joomla2drupal_vars['sandbox']['comment'];

  // we don't import comments marked as spam
  joomla2drupal_db_joomla_();
  $result = db_query_range("SELECT * FROM %sjomcomment WHERE published = 1 ORDER BY id", $_joomla2drupal_vars['config']['Joomla']['db_prefix'], $start, J2D_MAX_COMMENT);
  $comments = array();
  while ($row = db_fetch_array($result)) {
    $comments[] = $row;
  }
  joomla2drupal_db_drupal_();

  foreach ($comments as $comment) {
    $_joomla2drupal_vars['sandbox']['comment']++;
    $edit = array(
      'pid'  => 0,
      'nid'  => $comment['contentid'],
      'uid'  => joomla2drupal_convert_userid($comment['user_id']),
      'subject'  => joomla2drupal_process_text_($comment['title']),
      'comment'  => joomla2drupal_process_text_($comment['comment'], 'comment'),
      'status'  => 1 - $comment['published'],
      'name'  => $comment['name'],
      'mail'  => $comment['email'],
      'homepage'  => $comment['website'],
      'timestamp' => joomla2drupal_date_2_timestamp_($comment['date']),
      'ip' => $comment['ip'],
    );

    _joomla2drupal_comment_save_($edit);
  }
  $context['message'] = sprintf('Importing comments. Finished: %d/%d.', $_joomla2drupal_vars['sandbox']['comment'], $_joomla2drupal_vars['num']['comment']);
}

/**
 * Return plugin info
 */
function j2d_jomcomment_info() {
  global $_joomla2drupal_vars;

  if (! db_table_exists($_joomla2drupal_vars['config']['Joomla']['db_prefix'] .'jomcomment')) {
    return FALSE;
  }
  $batch = array();
  $count = db_result(db_query('SELECT COUNT(*) FROM %sjomcomment WHERE object_group="com_content"', $_joomla2drupal_vars['config']['Joomla']['db_prefix']));
  for ($i=0; $i<$count/J2D_MAX_COMMENT; $i++) {
    $batch[] = array('joomla2drupal_plugin', array('jomcomment', 'batch'));
  }
  $batch[] = array('joomal2drupal_rebuild_node_comment_statistics', array());
  
  return array('count' => $count, 'batch' => $batch);
}