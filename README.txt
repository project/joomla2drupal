Project page http://drupal.org/project/joomla2drupal

== INSTALL
- Go to admin/build/modules and enable this module
- Put your Joomla! configuration.php into the module folder
  if you want prefilled database information. Useful for testing
  the conversion

== USAGE
- Don't forget to back up your Drupal database
- Go to admin/joomla2drupal and follow instructions

== CUSTOMISATION
I leave a few files that I used when converting my Joomla! site,
including mambots (Joomla! plugins) conversion, SOBI2-to-CCK
conversion. However those things need code modification.

== NOTE
- Your Drupal database will be cleared, except for settings tables
- Modify plugins/bbcode.inc to fit your need (if necessary)